<?php

if (!file_exists(__DIR__ . "/../config.php")) {
    echo "Copy config.php.dist to config.php and update the CLIENT_URL and CLIENT_API_KEY constants.";
    die;
}

require_once __DIR__ . "/../config.php";
require_once __DIR__ . "/../class/ClientRecognitionSdk.php";

//Ensure we define a file
if (!isset($argv[1])) {
    echo "Please define the file you want to process e.g 'php uploadEmployeeFile.php employeeFile.xml'.\n";
    die;
}

$clientRecognitionSdk = new ClientRecognitionSdk(CLIENT_URL, CLIENT_API_KEY);

echo $clientRecognitionSdk->uploadEmployeeFile($argv[1]);
