<?php

if (!file_exists(__DIR__ . "/../config.php")) {
    echo "Copy config.php.dist to config.php and update the CLIENT_URL and CLIENT_API_KEY constants.";
    die;
}

require_once __DIR__ . "/../config.php";
require_once __DIR__ . "/../class/ClientRecognitionSdk.php";

//Ensure we define a file
if (!isset($argv[1])) {
    echo "Please define the file you want to process e.g 'php validateEmployeeFileSchema.php employeeFile.xml' .\n";
    die;
}

$clientRecognitionSdk = new ClientRecognitionSdk(CLIENT_URL, CLIENT_API_KEY);
$errors               = $clientRecognitionSdk->validateEmployeeFileSchema($argv[1]);

if ($errors) {
    echo "\nValidate employee file against schema failed with the following errors:\n";
    foreach ($errors as $error) {
        echo $error;
    }
    echo "\n";
} else {
    echo "\nValidate employee file against schema successful.\n\n";
}

