# PHP API Examples
The PHP examples provided here will give you examples of how to interact with the Workstars API. In particular, how to validate XML files and submit them to our HR Data Sync process.

## Getting Started
These instructions will get you a copy of the examples up and running on your local machine for development and testing purposes.

### Installing
To clone these examples use:

    git clone git@bitbucket.org:workstars/php-api-examples.git

### Credentials
Before you can call the API with one of our example scripts you must know your scheme URL and API-KEY.

Your scheme URL is the URL that is used to access the employee login page, for example "https://your-sub-domain.workstars.com"
    
The API-KEY must be generated from your administration portal. Login and go to **Settings > API** and click the "Generate Full Access Key" button.

### Setup
Open the file **config.php.dist** and enter the credentials from above and save it as **config.php**

## Example Scripts
In the scripts folder there are 5 example scripts:

### tlsCheck.php
This will check what default version of TLS is available. Our API requires TLS 1.2 or better, you may need to update your TLS/SSL libraries and/or OpenSSL.

Command line example:

    php scripts/tlsCheck.php


### validateEmployeeFileSchema.php
This will connect to our API and retrieve our latest xml schema for the employee file. It will then use it to validate your file and either return success or the validation errors.

Command line example:

    php scripts/validateEmployeeFileSchema.php default_employees.xml

### validatePositionFileSchema.php
This will connect to our API and retrieve our latest xml schema for the position file. It will then use it to validate your file and either return success or the validation errors.

Command line example:

    php scripts/validatePositionFileSchema.php default_positions.xml

### uploadEmployeeFile.php
This will do the same as the employee file validation example above but if the validation suceeds it will post the file to our API.

Command line example:

    php scripts/uploadEmployeeFile.php default_employees.xml

### uploadPositionFile.php
This will do the same as the position file validation example above but if the validation suceeds it will post the file to our API.

Command line example:

    php scripts/uploadPositionFile.php default_positions.xml

## Example XML Files
We have included the following sample files so you can you try the xml schema validation:

* default_employees.xml
* default_positions.xml

IMPORTANT: You should only use the above files with the validation examples. If you use them with the upload examples they will be uploaded and processed. The employee file will be rejected by our system as the email address examples are invalid but the position file will be processed.

