<?php

class ClientRecognitionSdk
{
    const EMPLOYEE_SCHEMA_ENDPOINT = "/admin/api/v2/hrdatasync/employees.xsd";
    const POSITION_SCHEMA_ENDPOINT = "/admin/api/v2/hrdatasync/positions.xsd";
    const EMPLOYEE_ENDPOINT = "/admin/api/v2/hrdatasync/employee";
    const POSITION_ENDPOINT = "/admin/api/v21/hrdatasync/position";

    /**
     * @var string
     */
    protected $url;
    /**
     * @var string
     */
    protected $apiKey;

    /**
     * ClientRecognitionSdk constructor.
     *
     * @param string $url
     * @param string $apiKey
     */
    public function __construct($url, $apiKey)
    {
        $this->url    = $url;
        $this->apiKey = $apiKey;
    }

    /**
     * @param $file
     * @return array
     */
    public function validateEmployeeFileSchema($file)
    {
        if (!$this->checkFileExists($file)) {
            $this->errorFileNotExists();
            die;
        }

        $schema = $this->curl($this->getEmployeeSchemaUrl());

        return $this->validateXMLSchema($schema, $file);
    }

    /**
     * @param $file
     * @return array
     */
    public function validatePositionFileSchema($file)
    {
        if (!$this->checkFileExists($file)) {
            $this->errorFileNotExists();
            die;
        }

        $schema = $this->curl($this->getPositionSchemaUrl());

        return $this->validateXMLSchema($schema, $file);
    }

    /**
     * @param $file
     * @return bool
     */
    public function uploadEmployeeFile($file)
    {
        if (!$this->checkFileExists($file)) {
            return $this->errorFileNotExists();
        }

        if (!$this->isEmployeeFileSchemaValid($file)) {
            echo "\nError: Employee file did not pass schema validation.\n\n";
            die;
        }

        $url = $this->getEmployeeUrl();

        return $this->curl($url, $file);
    }

    /**
     * @param $file
     * @return bool
     */
    public function uploadPositionFile($file)
    {
        if (!$this->checkFileExists($file)) {
            return $this->errorFileNotExists();
        }

        if (!$this->isPositionFileSchemaValid($file)) {
            echo "\nError: Position file did not pass schema validation.\n\n";
            die;
        }

        $url = $this->getPositionUrl();

        return $this->curl($url, $file);
    }

    /**
     * @return string
     */
    protected function getEmployeeSchemaUrl()
    {
        return $this->url . self::EMPLOYEE_SCHEMA_ENDPOINT;
    }

    /**
     * @return string
     */
    protected function getPositionSchemaUrl()
    {
        return $this->url . self::POSITION_SCHEMA_ENDPOINT;
    }

    /**
     * @return string
     */
    protected function getEmployeeUrl()
    {
        return $this->url . self::EMPLOYEE_ENDPOINT;
    }

    /**
     * @return string
     */
    protected function getPositionUrl()
    {
        return $this->url . self::POSITION_ENDPOINT;
    }

    /**
     * @param $file
     * @return bool
     */
    protected function checkFileExists($file)
    {
        return file_exists($file);
    }

    /**
     * @return bool
     */
    protected function errorFileNotExists()
    {
        echo "File does not exist\n";

        return false;
    }

    /**
     * @param $schema
     * @param $file
     * @return array
     */
    private function validateXMLSchema($schema, $file)
    {
        libxml_clear_errors();
        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadXML(file_get_contents($file));

        $errors = [];

        if (!$dom->schemaValidateSource($schema)) {
            $validationErrors = libxml_get_errors();
            libxml_clear_errors();
            foreach ($validationErrors as $validationError) {
                $errors[] = "Line #" . $validationError->line . ": " . $validationError->message;
            }
        }

        return $errors;
    }

    /**
     * @param string $url
     * @param string $file
     * @return mixed
     */
    private function curl($url, $file = "")
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERPWD, "api-key:$this->apiKey");
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($file) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, ['file' => new CURLFile($file)]);
            if (7 != PHP_MAJOR_VERSION) {
                curl_setopt($curl, CURLOPT_SAFE_UPLOAD, 1);
            }
        }

        $cResponse = curl_exec($curl);
        $httpCode  = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpCode != 200) {
            echo "\nError: Curl response was not successful. HTTP Code '$httpCode' received from URL '$url'.\n\n";
            echo "Response: " . $cResponse . "\n\n";
            die;
        }

        return $cResponse;
    }

    /**
     * @param $file
     * @return bool
     */
    private function isPositionFileSchemaValid($file)
    {
        return empty($this->validatePositionFileSchema($file));
    }

    /**
     * @param $file
     * @return bool
     */
    private function isEmployeeFileSchemaValid($file)
    {
        return empty($this->validateEmployeeFileSchema($file));
    }
}
